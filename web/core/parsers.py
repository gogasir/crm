from rest_framework_xml.parsers import XMLParser


class ListXMLParser(XMLParser):
    list_tags = [
        'targets',
    ]

    def _xml_convert(self, element):
        """
        convert the xml `element` into the corresponding python object
        """

        children = list(element)

        if len(children) == 0:
            return self._type_convert(element.text)
        else:
            # if the fist child tag is list-item means all children are list-item
            if children[0].tag == "target":
                data = []
                for child in children:
                    data.append(self._xml_convert(child))
            else:
                if element.tag == 'order' and element.attrib.get('reference'):
                    data = {'reference': element.attrib.get('reference')}
                else:
                    data = {}

                for child in children:
                    data[child.tag] = self._xml_convert(child)

            return data
