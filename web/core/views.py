import datetime

from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from core import service
from core.models import Order, Target, Payment
from core.parsers import ListXMLParser
from core.serializers import OrderSerializer, PaymentSerializer


class OrderView(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (ListXMLParser,)
    renderer_classes = (JSONRenderer,)

    def post(self, request):
        dict_targets = service.check_and_calculate(request.data)
        serializer_data = OrderSerializer(data=dict_targets)
        if serializer_data.is_valid(raise_exception=True):
            order = service.order_save(dict_targets, request.user)
            return Response({
                "start_date": order.start_date,
                "end_date": order.end_date,
                "hours": order.hours,
                "order": {
                    "reference": order.reference,
                    "amount_due": order.amount_due,
                },
            }, status=status.HTTP_201_CREATED)

        return Response(serializer_data.errors, status=status.HTTP_400_BAD_REQUEST)


class OrderDetailView(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)

    def post(self, request, reference):
        order = Order.objects.get(reference=reference)
        targets = Target.objects.filter(order_id=order.pk).order_by('-local_priority')
        targets_json = [t.as_json() for t in targets]
        return Response(targets_json)


class OrderPaymentView(APIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)

    def post(self, request):
        serializer_data = PaymentSerializer(data=request.data, many=True)
        response_payment = []
        if serializer_data.is_valid():
            for pay in serializer_data.validated_data:
                try:
                    order = Order.objects.get(reference=pay['reference'])
                except Order.DoesNotExist:
                    response_payment.append({
                        "reference": pay['reference'],
                        "status": "error",
                        "message": "Order reference not found"})
                    continue
                payment = order.payment_set.all().exists()
                if payment:
                    response_payment.append({
                        "reference": pay['reference'],
                        "status": "error",
                        "message": "Already paid"})
                    continue
                amount_due = order.amount_due
                incoming_amount = pay['amount_due']
                if amount_due != incoming_amount:
                    response_payment.append({
                        "reference": pay['reference'],
                        "status": "error",
                        "message": f'Not equal to order amount {order.amount_due}'})
                    continue
                payment = Payment()
                payment.order = order
                payment.amount = incoming_amount
                payment.save()
                response_payment.append({
                    "reference": pay['reference'],
                    "status": "success",
                    "message": 'Paid'})

            return Response(response_payment, status=status.HTTP_200_OK)

        return Response(serializer_data.errors, status=status.HTTP_400_BAD_REQUEST)
