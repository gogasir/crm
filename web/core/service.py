import datetime

from django.db import transaction

from core.models import Order, Target


def check_and_calculate(data):
    # TODO Клиент не может заказывать других ваших клиентов (отмена заказа).
    targets = data.get('targets')
    targets_sorted = sorted(targets, key=lambda k: ('+' if k['link'] is None else '', k['priority']), reverse=True)
    dict_targets = dict()
    for val in targets_sorted:
        val['local_difficulty'] = val['difficulty']
        val['local_priority'] = None
        dict_targets[val['username']] = val

    q = len(targets)
    for d in targets_sorted:
        username = d['username']
        if not dict_targets[username]['local_priority'] is None:
            q = calculate_parent(username, dict_targets, q)
        else:
            q = calculate_parent(username, dict_targets, q)
            dict_targets[username]['local_priority'] = q
            q -= 1
    data['hours'], data['amount_due'] = calculate_price(dict_targets)
    start_date = datetime.datetime.now()
    end_date = start_date + datetime.timedelta(hours=data['hours'])
    data['start_date'], data['end_date'] = start_date, end_date
    data['targets'] = [*dict_targets.values()]
    return data


def calculate_price(dict_targets):
    hour = 0
    for x, y in dict_targets.items():
        hour += y['local_difficulty']
    price = hour * 10
    return hour, price


def calculate_parent(username: str, dict_targets: dict, q: int):
    link = dict_targets[username]['link']
    if link:
        parent = dict_targets[link]
        if parent['local_priority'] is None:
            dict_targets[link]['local_priority'] = q
            q -= 1
            dict_targets[link]['local_difficulty'] = calculate_difficulty(dict_targets[link])
        else:
            dict_targets[link]['local_difficulty'] = calculate_difficulty(dict_targets[link])
    return q


def calculate_difficulty(target: dict):
    if target['age'] >= 40:
        return target['local_difficulty'] * 2
    else:
        return target['local_difficulty'] * 1.5


@transaction.atomic()
def order_save(dict_targets, user):
    order = Order()
    order.reference = dict_targets['reference']
    order.start_date = dict_targets['start_date']
    order.end_date = dict_targets['end_date']
    order.hours = dict_targets['hours']
    order.amount_due = dict_targets['amount_due']
    order.client = user
    order.save()
    for target in dict_targets['targets']:
        t = Target()
        t.order = order
        t.username = target['username']
        t.age = target['age']
        t.priority = target['priority']
        t.difficulty = target['difficulty']
        t.link = target['link']
        t.local_priority = target['local_priority']
        t.local_difficulty = target['local_difficulty']
        t.save()
    return order
