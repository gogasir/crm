from django.db import models
from django.contrib.auth.models import User


class Order(models.Model):
    """
        Order
    """
    reference = models.CharField(max_length=150, unique=True, db_index=True)
    client = models.ForeignKey(
        to=User,
        verbose_name=u'Пользователь',
        on_delete=models.CASCADE,)
    created_at = models.DateTimeField(auto_now_add=True)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    hours = models.IntegerField(default=0)
    amount_due = models.IntegerField(
        default=0,
        verbose_name=u'Сумма к оплате'
    )


class Target(models.Model):
    """
        Target
    """
    username = models.CharField(max_length=150,
                                db_index=True,
                                verbose_name=u'Имя')
    age = models.IntegerField(default=1, verbose_name=u'Возраст')
    priority = models.IntegerField(default=1, verbose_name=u'Приоритет')
    difficulty = models.IntegerField(default=1, verbose_name=u'Сложность')
    link = models.CharField(max_length=150, null=True)
    local_priority = models.IntegerField(verbose_name=u'Порядок выполнения')
    local_difficulty = models.FloatField(verbose_name=u'Время на убийство')
    order = models.ForeignKey(
        to=Order,
        related_name='target_%(app_label)s_%(class)s_set',
        on_delete=models.CASCADE)
    time_of_death = models.DateTimeField(blank=True,
                                         null=True,
                                         verbose_name=u'Время смерти')

    def as_json(self):
        return dict(
            username=self.username,
            age=self.age,
            priority=self.local_priority,
            link=self.link,
            difficulty=self.local_difficulty,
            time_of_death=self.time_of_death,
        )


class Payment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField(
        default=0,
        verbose_name=u'Сумма оплаты'
    )
    order = models.ForeignKey(
        to=Order,
        unique=True,
        on_delete=models.CASCADE,
    )
