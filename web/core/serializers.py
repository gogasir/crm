from django.contrib.auth.models import User
from django.db import transaction
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from core.models import Target, Order


class TargetSerializer(serializers.Serializer):
    """
        Target Serializer
    """

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        return Target(**validated_data)

    username = serializers.CharField()
    age = serializers.IntegerField()
    priority = serializers.IntegerField()
    difficulty = serializers.IntegerField()
    link = serializers.CharField(allow_null=True)


class OrderSerializer(serializers.Serializer):
    """
        Order Serializer
    """

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    targets = serializers.ListField(child=TargetSerializer())
    reference = serializers.CharField(max_length=20)

    def validate_reference(self, value):
        if Order.objects.filter(reference=value).exists():
            raise serializers.ValidationError('Order with this reference already exists.')

    def validate_targets(self, value):
        if len(value) == 0:
            raise serializers.ValidationError('Targets must not be empty.')


class PaymentSerializer(serializers.Serializer):
    amount_due = serializers.FloatField()
    reference = serializers.CharField()


