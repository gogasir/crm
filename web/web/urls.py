from django.contrib import admin
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from core import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/auth/', obtain_auth_token, name='api_token_auth'),
    path('api/order/payment/', views.OrderPaymentView.as_view(), name='view_order_payment'),
    path('api/order/<str:reference>/', views.OrderDetailView.as_view(), name='view_order_detail'),
    path('api/order/', views.OrderView.as_view(), name='view_order'),


]
