# Use an official Python runtime as a parent image
FROM python:3.8

# Env folder
ENV HOME=/home/app
ENV APP_HOME=/home/app/web

# MkDir
RUN mkdir -p $HOME
RUN mkdir -p $APP_HOME

# Work dir
WORKDIR $APP_HOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install any needed packages specified in requirements.txt
COPY wait-for-it.sh $HOME
COPY requirements.txt $APP_HOME

# install dependencies
RUN pip install -r requirements.txt