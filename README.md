# Run project
Build docker
```bash
make build 
```
Start app
```bash
make up
```
If first run app
```bash
make migrate
make collect
make admininit
```

Rest api

- POST api/auth/ authentication by username and password
```bash
curl -i -X POST \
   -H "Content-Type:application/json" \
   -d \
'{"username":"admin", "password":"admin"}' \
 'http://127.0.0.1/api/auth/'
```
- POST api/order/ create new order
```bash
curl -i -X POST \
   -H "Content-Type:application/xml" \
   -H "Authorization:Token fcb73464d19f5afe71de5be73be964417d8df929" \
   -d \
'<order reference="CORLEONE">
  <targets>
    <target>
      <username>michael</username>
      <age>45</age>
      <priority>1</priority>
      <difficulty>3</difficulty>
      <link></link>
    </target>
    <target>
      <username>tomhagen</username>
      <age>35</age>
      <priority>1</priority>
      <difficulty>1</difficulty>
      <link>michael</link>
    </target>
    <target>
      <username>frankhagen</username>
      <age>23</age>
      <priority>4</priority>
      <difficulty>2</difficulty>
      <link>tomhagen</link>
    </target>
    <target>
      <username>peteclemenza</username>
      <age>45</age>
      <priority>2</priority>
      <difficulty>10</difficulty>
      <link>michael</link>
      </target>
    <target>
      <username>fredo</username>
      <age>50</age>
      <priority>10</priority>
      <difficulty>1</difficulty>
      <link>peteclemenza</link>
    </target>
  </targets>
</order>
' \
 'http://127.0.0.1/api/order/'
```

- POST api/order/<str:reference>/ view detail order 
```bash
curl -i -X POST \
   -H "Content-Type:application/xml" \
   -H "Authorization:Token bfd709cb671b3d677dda765bffb3f2b5c310384f" \
   -d \
'' \
 'http://127.0.0.1/api/order/CORLEONE_1/'
```

- POST api/order/payment/ payment order
```bash
curl -i -X POST \
   -H "Content-Type:application/json" \
   -H "Authorization:Token bfd709cb671b3d677dda765bffb3f2b5c310384f" \
   -d \
'[
  {
  "reference": "CORLEONE_1",
  "amount_due":365
},{
  "reference": "CORLEONE_2",
  "amount_due":365
}]' \
 'http://127.0.0.1/api/order/payment/'
```



