PROJECT_NAME=wall

up:
	@docker-compose up -d

stop:
	@docker-compose stop

down:
	@docker-compose down

build:
	@docker-compose build

build_force:
	@docker-compose build --force-rm

ps:
	@docker-compose ps

migrate:
	@docker-compose exec app python manage.py migrate --noinput

collect:
	@docker-compose exec app python manage.py collectstatic --no-input --clear

initadmin:
	@docker-compose exec app python manage.py initadmin

exec:
	@docker exec -it app /bin/bash

logs:
	@docker-compose logs -f